<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class HomeControllerTest extends WebTestCase
{
    public function testUser()
    {
        $client = static::createClient();
        $client->followRedirects();
        $crawler = $client->request('GET', '/login');
        $btnSubmit = $crawler->selectButton('Connexion');
        $form=$btnSubmit->form([

            'email'=>'12',
            'password'=>'$argon2id$v=19$m=65536,t=4,p=1$V0c0ALnRG9B28NtY2qPX9Q$2NpQO+MtzZ5QJx8fHXSIbg6d/eaHI/5YIsnZhugD7uU']          );

        $client->submit($form);
        $this->assertResponseIsSuccessful();

    }

}
