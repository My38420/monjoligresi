<?php

namespace App\Repository;

use App\Entity\Places;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Places|null find($id, $lockMode = null, $lockVersion = null)
 * @method Places|null findOneBy(array $criteria, array $orderBy = null)
 * @method Places[]    findAll()
 * @method Places[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlacesRepository extends ServiceEntityRepository
{
    public $sortFindAll;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Places::class);
    }

    // /**
    //  * @return Places[] Returns an array of Places objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Places
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    /**
     * @return Places[] Returns an array of Places objects
     */
    public function sortFindAll(): array
    {
        $qb = $this->createQueryBuilder('a')
            ->addOrderBy('a.type', 'ASC')
            ->addOrderBy('a.commune', 'ASC')
            ->addOrderBy('a.title', 'ASC');

        $query = $qb->getQuery();

        return $query->execute();
    }

    /**
     * @return Places[] Returns an array of Places objects
     */
    public function FindAllPlaces($value): array
    {
        $qb = $this->createQueryBuilder('b')
            ->andWhere('b.type = :val')
            ->setParameter('val', $value)
            ->addOrderBy('b.commune', 'ASC')
            ->addOrderBy('b.title', 'ASC');

        $query = $qb->getQuery();
        return $query->execute();
    }

}
