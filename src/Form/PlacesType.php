<?php

namespace App\Form;

use App\Entity\Places;
use phpDocumentor\Reflection\Types\Integer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\Positive;
use Symfony\Component\Validator\Constraints as Assert;
class PlacesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title',null, [
        'label' => 'Titre'
    ])
            ->add('commune')
            ->add('description')
            ->add('start_point',null, [
                'label' => 'Point de départ', 'attr' => [
                    'placeholder' => 'hameau, lieu dit, parking....'
                ]
            ])
            ->add('interest_point',null, [
                'label' => 'Intérêts', 'attr' => [
                    'placeholder' => 'hameau, lieu dit, parking....'
                ]
            ])
            ->add('distance', null,
                [
                'label' => 'Distance en km',
                    'attr' => [
                        'placeholder' => 'distance affichée uniquement pour les balades',
        'min' => '0'
    ]])
            ->add('time', null, [
                'label' => 'Temps en minutes',
                'attr' => [
                    'min' => '0',
                    'placeholder' => 'temps affiché uniquement pour les balades'
            ]])
            ->add('type', ChoiceType::class, [ 'label' => 'Type : "balade" si marche à faire, "autre" sinon',
                'choices' => [
                    'autre' => 'autre',
                    'balade' => 'balade',
                                  ],
            ])
            ->add('difficulty', null, [
        'label' => 'Difficulté',
                'attr' => [
                    'placeholder' => 'par exemple, indication si montée difficile'
                ]
    ])
            ->add('imageFile', VichImageType::class, [
                'delete_label' => 'Supprimer',
                'label_attr' => ['lang' => 'fr',
                    'Browse' => 'Parcourir'],
                'download_label' => static function (Places $balades) {
                    return $balades->getImageName();},
                 'attr' => ['lang' => 'fr',
                   'placeholder' => 'Téléchargez une photo si vous voulez'
                ],
             'required' => false,
                'label' => 'Image',
                'allow_file_upload' => true,
                'allow_delete' => true])
              ->add('published', null, [
                  'label' => 'Publié'
              ])
        ;
               }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Places::class
        ]);
    }

   }
