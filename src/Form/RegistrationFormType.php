<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email')

            ->add('plainPassword', PasswordType::class, [
                // instead of being set onto the object directly,
                // this is read and encoded in the controller
                'mapped' => false,
                'label' => 'mot de passe',

                'constraints' => [
                    new NotBlank([
                        'message' => 'Entrez un mot de passe',
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'Votre mot de passe doit contenir au moins {{ limit }} caractères',
                        // max length allowed by Symfony for security reasons
                        'max' => 255,
                        'maxMessage' => 'Votre mot de passe est trop long !'
                    ]),
                ],
            ])
            ->add('name', null,  ['label' => 'Nom'])
            ->add('lastname', null,  ['label' => 'Prénom'])
            ->add('pseudo', TextType::class, ['constraints' => [
                new NotBlank([
                   'message' => 'Entrez un pseudo',
             ]),
             new Length([
                 'min' => 2,
                 'minMessage' => 'Votre pseudo doit contenir au moins {{ limit }} characters',
                 // max length allowed by Symfony for security reasons
                 'max' => 180,
             ]),
            ]])
            ->add('agreeTerms', CheckboxType::class, [
                'mapped' => false,
                'label' => ' accepter les conditions',
                'constraints' => [
                    new IsTrue([
                        'message' => 'Vous devez agréer les conditions',
                    ]),
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
