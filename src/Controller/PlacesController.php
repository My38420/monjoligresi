<?php

namespace App\Controller;

use App\Entity\Places;
use App\Entity\Comments;
use App\Entity\User;
use App\Form\PlacesType;
use App\Form\CommentType;
use App\Form\CommenType;
use App\Repository\PlacesRepository;
use Doctrine\ORM\EntityManagerInterface;
use PhpParser\Node\Stmt\Foreach_;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\DateTime;
use Vich\UploaderBundle\Entity\File;

/**
 * @Route("/balades")
 */
class PlacesController extends AbstractController
{
    /**
     * @Route("/", name="baladesListe", methods={"GET"})
     */
    public function index(PlacesRepository $placesRepository): Response
    {
        /**
         * affiche tous les lieux enregistrés en bdd triées par type (autre/balade)
         * puis par commune
         */
        return $this->render('places/index.html.twig', [
            'places' => $placesRepository->sortFindAll(),
        ]);
    }
    /**
     * @Route("/listeBalades", name="listeBalades", methods={"GET"})
     */
    public function listPlaces(PlacesRepository $placesRepository): Response
    {
        /**
         * affiche toutes les balades enregistrés en bdd triées par commune
         */
        return $this->render('places/indexPlaces.html.twig', [
            'places' => $placesRepository->FindAllPlaces('balade'),
        ]);
    }
    /**
     * @Route("/listeAutres", name="listeAutres", methods={"GET"})
     */
    public function listOthers(PlacesRepository $placesRepository): Response
    {
        /**
         * affiche tous les autres lieux à voir enregistrés en bdd tries par commune
         */
        return $this->render('places/indexOthers.html.twig', [
            'places' => $placesRepository->FindAllPlaces('autre'),
        ]);
    }

    /**
     * @Route("/{id}", name="baladesAfficher", methods={"GET"})
     */
    public function show(Places $place): Response
    {
        /**
         * appel de la vue qui affiche le détail du lieu par son id
         */
        return $this->render('places/show.html.twig', [
            'place' => $place,
        ]);
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     * @Route("/{id}/edit", name="baladesModifier", methods={"GET","POST"})
     */
    public function edit(Request $request, Places $place): Response
    {
        /**
         * fonction réservée à l'admin qui récupère les données modifiées pour un lieu
         * et les enregistre en BDD
         */
        $form = $this->createForm(PlacesType::class, $place);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $published = $form['published']->getData();
            if ($published) {
                $place->setPublished(true) ;
             }
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('baladesListe');
        }

        return $this->render('places/edit.html.twig', [
            'place' => $place,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     * @Route("/{id}/delete", name="baladesSupprimer", methods={"DELETE"})
     */
    public function delete(Request $request, Places $place): Response
    {
        /**
         * fonction réservée à l'admin qui supprime un lieu de la BDD après confirmation
        */
        if ($this->isCsrfTokenValid('delete'.$place->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($place);
            $entityManager->flush();
        }

        return $this->redirectToRoute('baladesListe');
    }
    /**
     * @IsGranted("ROLE_MEMBER")
     * @Route("/envoyerCommentaire/{id}", name="envoyerCommentaire", methods={"GET","POST"})
     */
    public function sendComment(Request $request, Places $place, MailerInterface $mailer): Response
    {
        $form = $this->createForm(CommentType::class);
        $form->handleRequest($request);
        $user = $this->getUser();
        $usermail = $user->getUsername();
        if ($form->isSubmitted() && $form->isValid()) {
            /**
             *  On récupère les données transmises : le lieu sélectionné, l'utilisateur qui envoie
             *  le commentaire et le texte du commentaire saisi dans le formulaire
             */

            $placetitle= $place->getTitle();
            $comment = $form->get('commentaire')->getData();
            /**
             * préparation du mail avec le format préparé
             */
            $mail=(new TemplatedEmail())
                ->to ('myriam.trillat.simplon@gmail.com')
                ->from ($usermail)
                ->subject('Envoi commentaire')
                ->htmlTemplate('places/email.html.twig')
                // on passe les variables au template
                ->context([
                    'titrebalade' => $placetitle,
                    'mailexp' => $usermail,
                    'commentaire'=> $comment
                ])
            ;
            /**
             * envoie du mail et affichage d'un message de succès
             */
            $mailer->send($mail);
            if (!$this->isGranted("ROLE_ADMIN")) {
                $this->addFlash('message', 'Commentaire envoyé par mail avec succès');
            }
            return $this->redirectToRoute("baladesListe");
        }
        return $this->render('places/sendComment.html.twig', [
            'place' => $place,
            'form' => $form->createView()
        ]);
    }
    /**
     * @IsGranted("ROLE_MEMBER")
     * @Route("/baladesProposer/{user}", name="baladesProposer", methods={"GET","POST"})
     *
     */
    public function proposePlace(Request $request, User  $user, MailerInterface $mailer): Response
    {
        /**
         * instanciation d'une nouvelle class Places et appel du formulaire de saisie
         * créé à partir du modèle de la classe Places
         */
        $place = new Places();
        $form = $this->createForm(PlacesType::class, $place);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $place->setPublished(false);
            /**
             * si les données saisies pour ajouter un nouveau lieu sont correctes,
             * affectation des données à la nouvelle instanciation de classe
             * par défaut l'état publié est à false
             */
                if ($this->isGranted("ROLE_ADMIN")){
                    /**
                     * pour le role admin, l'état publié est à true pour afficher directement ce lieu
                     */
                    $place->setPublished(true);
                }
            /**
             * les données sont prêtes à être enregistrées
             */
            $entityManager->persist($place);
            /**
             * les données sont enregistrées dans la bdd
             */
            $entityManager->flush();

            if (!$this->isGranted("ROLE_ADMIN")) {
                /**
                 *  Si c'est juste un utilisateur de role membre qui propose un nouveau lieu,
                 * on récupère les données transmises dans le formulaire pour envoyer un mail
                 * à l'admin
                 */

                $mailexp = $user->getEmail();
                $placetitle = $form->get('title')->getData();
                $commune = $form->get('commune')->getData();
                $description = $form->get('description')->getData();
                $type = $form->get('type')->getData();
                $time = $form->get('time')->getData();
                $depart = $form->get('start_point')->getData();
                $interest = $form->get('interest_point')->getData();
                $distance = $form->get('distance')->getData();
                $difficulty = $form->get('difficulty')->getData();
                /**
                 * utilisation d'un template de formatage du mail
                 */
                $mail = (new TemplatedEmail())
                    ->to('myriam.trillat.simplon@gmail.com')
                    ->from($mailexp)
                    ->subject('Envoi balade')
                    ->htmlTemplate('places/emailPlace.html.twig')
                    ->context([
                        'mailexp' => $mailexp,
                        'titrebalade' => $placetitle,
                        'commune' => $commune,
                        'description' => $description,
                        'depart' => $depart,
                        'interet' => $interest,
                        'temps' => $time,
                        'distance' => $distance,
                        'type' => $type,
                        'difficulte' => $difficulty

                    ]);
                /**
                 * envoi du mail préparé via la fonction send
                 */
                $mailer->send($mail);
                /**
                 * affichage d'un message de bon envoi du mail à l'utilisateur
                 */
                $this->addFlash("message", "Votre balade a bien été enregistrée, elle sera publiée après contrôle");

            }
            /**
             * après l'ajout d'une balade, on revient sur la liste complète des lieux
             */
            return $this->redirectToRoute('baladesListe');
        }

        return $this->render('places/new.html.twig', [
            'place' => $place,
            'form' => $form->createView(),
        ]);

    }

}
