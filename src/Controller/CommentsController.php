<?php

namespace App\Controller;

use App\Entity\Comments;
use App\Entity\Places;
use App\Form\CommenType;
use App\Repository\CommentsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;


/**
 *
 * @Route("/comments")
 */
class CommentsController extends AbstractController
{
    /**
     * @IsGranted("ROLE_MEMBER")
     * @Route("/ajoutCommentaire/{id}", name="ajoutCommentaire", methods={"GET","POST"})
     */
    public function addComment(Request $request, Places $place, EntityManagerInterface $entityManager, MailerInterface $mailer): Response
//    public function addComment(EntityManagerInterface $entityManager, Places $place, Request $request)
    {
        $comment = new Comments();
        $form = $this->createForm(CommenType::class, $comment);
        $form->handleRequest($request);
        $user = $this->getUser();
        $usermail = $user->getUsername();
        if ($form->isSubmitted() && $form->isValid()) {
            /**
             *  On récupère les données transmises : le lieu sélectionné, l'utilisateur qui envoie
             *  le commentaire et le texte du commentaire saisi dans le formulaire
             * le commentaire saisi est créé dans la table comments avec un lien sur le lieu
             */
            $entityManager = $this->getDoctrine()->getManager();
            $comment->setPlace($place);
            $comment->setCreatedat(new \DateTime());
            if (!$this->isGranted("ROLE_ADMIN")) {
                $comment->setPublished(false);
            }
            else {
                $comment->setPublished(true);
            }
            $entityManager->persist($comment);
            $entityManager->flush();
            if (!$this->isGranted("ROLE_ADMIN")) {
                $placetitle = $place->getTitle();
                $comment = $form->get('textcomment')->getData();
                /**
                 * préparation du mail avec le format préparé
                 */
                $mail = (new TemplatedEmail())
                    ->to('myriam.trillat.simplon@gmail.com')
                    ->from($usermail)
                    ->subject('Envoi commentaire')
                    ->htmlTemplate('comments/email.html.twig')
                    // on passe les variables au template
                    ->context([
                        'titrebalade' => $placetitle,
                        'mailexp' => $usermail,
                        'commentaire' => $comment
                    ]);
                /**
                 * envoie du mail et affichage d'un message de succès
                 */
                $mailer->send($mail);
                $this->addFlash('message', 'Commentaire envoyé par mail avec succès');
            }
            return $this->redirectToRoute("baladesListe");
        }

        return $this->render('comments/sendComment.html.twig', [
            'place' => $place,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/", name="comments_index", methods={"GET"})
     */
    public function index(CommentsRepository $commentsRepository): Response
    {
        return $this->render('comments/index.html.twig', [
            'comments' => $commentsRepository->findAll(),
        ]);
    }
    /**
     * @IsGranted("ROLE_ADMIN")
     * @Route("/publish/{id}", name="publishComment", methods={"GET","POST"})
     */
    public function publishComment(Request $request, Comments $comment): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $comment->setPublished(true);
        $entityManager->flush();
        return $this->redirectToRoute('baladesListe');
    }
    /**
     * @IsGranted("ROLE_ADMIN")
     * @Route("/new", name="comments_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $comment = new Comments();
        $form = $this->createForm(CommenType::class, $comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($comment);
            $entityManager->flush();

            return $this->redirectToRoute('baladesListe');
        }

        return $this->render('comments/new.html.twig', [
            'comment' => $comment,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="comments_show", methods={"GET"})
     */
    public function show(Comments $comment): Response
    {
        return $this->render('comments/show.html.twig', [
            'comment' => $comment,
        ]);
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     * @Route("/{id}/edit", name="comments_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Comments $comment): Response
    {
        $form = $this->createForm(CommentsType::class, $comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('comments_index');
        }

        return $this->render('comments/edit.html.twig', [
            'comment' => $comment,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     * @Route("/{id}/delete", name="comments_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Comments $comment): Response
    {
        if ($this->isCsrfTokenValid('delete'.$comment->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($comment);
            $entityManager->flush();
        }

        return $this->redirectToRoute('baladesListe');
    }
}
